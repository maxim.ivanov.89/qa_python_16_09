from abc import ABC
from random import randint

from faker import Faker

fake = Faker(locale='uk_UA')


class SchoolStaff(ABC):
    """
    Абстраткний класс. Повертає name та salary
    """
    def __init__(self, name, salary):
        self.name = name
        self.salary = salary

    def __str__(self):
        return f'{self.name} | {self.salary}'


class TeachersStuff(SchoolStaff):
    """
    Клас що наслідується від абстрактного класу SchoolStaff, повертає ім‘я та зп вчителя
    """
    def __str__(self):
        return f'{self.name} | {self.salary}'


class TechnicalsStuff(SchoolStaff):
    """
    Клас що наслідується від абстрактного класу SchoolStaff, повертає ім‘я та зп технічного співробітника
    """
    def __str__(self):
        return f'{self.name} | {self.salary}'


class School:
    def __init__(self, title, principal: TeachersStuff, teachers_stuff_list=5, technicals_stuff_list=3):
        """
        Args:
            title: назва школи
            principal: директор
            teachers_stuff_list: генерація вчительского складу
            technicals_stuff_list: генерація складу технічного персоналу
        """
        self.title = title
        self.principal = principal
        self.teachers_stuff_list = [TeachersStuff(fake.name(), randint(10000, 50000)) for position in
                                    range(teachers_stuff_list)]
        self.technicals_stuff_list = [TechnicalsStuff(fake.name(), randint(5000, 12000)) for position in
                                      range(technicals_stuff_list)]

    @property
    def total_salary(self):
        """
        Returns: повертає загальну заробітну плату усіх співробітників школи
        """
        staff_salary = [self.principal]
        staff_salary += self.teachers_stuff_list
        staff_salary += self.technicals_stuff_list
        total = sum(obj.salary for obj in staff_salary)
        return total

    def set_new_principal(self):
        """
        Метод випадковим чиніом змінює директора
        При призначенні нового директора кандидат видаляється зі списку вчителів,
        а старий директор додається до списку вчителів
        """
        # looks previous principal
        print(self.principal)
        old_principal = self.principal
        # random choise
        candidate = self.teachers_stuff_list.pop()
        print(candidate)
        self.principal = candidate
        self.teachers_stuff_list.append(old_principal)

    def show_teachers_list(self):
        """
        Returns:список вчителів
        """
        for x in self.teachers_stuff_list:
            print("Teacher:", x)


school8 = School('ОШ 8', TeachersStuff('Петро Директорович', 1000000))
print(school8.total_salary)
school8.teachers_stuff_list.append(TeachersStuff('Анатолій Копіумний', 200))
school8.set_new_principal()
school8.show_teachers_list()

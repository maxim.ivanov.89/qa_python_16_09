# Дана довільна строка. Напишіть код, який знайде в ній і
# віведе на екран кількість слів, які містять дві голосні літери підряд.

text = 'двоокииисуу оу бааіоо, хмм'.lower().split()
checkList = set('аеєіиїоуюя')

resultWords = []

for word in text:
    prevLetter = False
    for letter in word:
        if letter in checkList:
            if prevLetter is False:
                prevLetter = True
            else:
                resultWords.append(word)
                break
        else:
            prevLetter = False

print(len(resultWords))

# Є два довільних числа які відповідають за мінімальну і максимальну ціну.
# Є Dict з назвами магазинів і цінами:
# { "cito": 47.999, "BB_studio" 42.999, "momo": 49.999, "main-service": 37.245,
# "buy.now": 38.324, "x-store": 37.166, "the_partner": 38.988, "sota": 37.720, "rozetka": 38.003}.
# Напишіть код, який знайде і виведе на екран назви магазинів,
# ціни яких попадають в діапазон між мінімальною і максимальною ціною. Наприклад:

# lower_limit = 35.9
# upper_limit = 37.339
# > match: "x-store", "main-service"

dict2 = {"cito": 47.999,
         "BB_studio": 42.999,
         "momo": 49.999,
         "main-service": 37.245,
         "buy.now": 38.324,
         "x-store": 37.166,
         "the_partner": 38.988,
         "rozetka": 38.003}

for key, value in dict2.items():
    if min(dict2.values()) < value < max(dict2.values()):
        print("store: ", key)


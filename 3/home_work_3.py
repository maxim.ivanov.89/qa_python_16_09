# Зформуйте строку, яка містить певну інформацію про символ в відомому слові.
# Наприклад "The [номер символу] symbol in [тут слово] is '[символ з відповідним порядковим номером]'".
# Слово та номер отримайте за допомогою input() або скористайтеся константою.
# Наприклад (слово - "Python" а номер символу 3) - "The 3 symbol in "Python" is 't' ".

while True:
    try:
        userInputWord = input('Choose your word: ')
        userInputInfo = int(input('Choose number: '))
        wordCounter = len(userInputWord)
        if wordCounter >= userInputInfo:
            print(f'The {userInputInfo} symbol in {userInputWord} is {userInputWord[userInputInfo - 1]}')
            break
        else:
            print('Out of range, please try again')
    except BaseException as e:
        print(e)

# Написати цикл, який буде вимагати від користувача ввести слово, в якому є буква "о" (враховуються як великі так і маленькі).
# Цикл не повинен завершитися, якщо користувач ввів слово без букви о.


while True:
    print("type word with letter 'o'\n")
    try:
        inputUser = input().lower()
        if 'o' not in inputUser:
            print("word is not contain 'o'\n")
        else:
            print('Word is contain o')
            break
    except BaseException as e:
        print(e)

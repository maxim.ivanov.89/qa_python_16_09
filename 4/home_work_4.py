# Є list з даними lst1 = ['1', '2', 3, True, 'False', 5, '6', 7, 8, 'Python', 9, 0, 'Lorem Ipsum'].
# Напишіть код, який сформує новий list (наприклад lst2), який містить лише змінні типу стрінг, які присутні в lst1.
# Зауважте, що lst1 не є статичним і може формуватися динамічно.

import copy

lst1 = ['1', '2', 3, True, 'False', 5, '6', 7, 8, 'Python', 9, 0, 'Lorem Ipsum']
lst2 = copy.deepcopy(lst1)

for item in lst2:
    if item != type(str):
        lst2.remove(item)

print(lst2)

# Є list довільних чисел, наприклад [11, 77, 4, 22, 0, 56, 5, 95, 7, 5, 87, 13, 45, 67, 44].
# Напишіть код, який видалить з нього всі числа, які менше 21 і більше 74.

lst3 = [11, 77, 4, 22, 0, 56, 5, 95, 7, 5, 87, 13, 45, 67, 44]

for item in lst3:
    if item < 21:
        lst3.remove(item)

for item in lst3:
    if item > 74:
        lst3.remove(item)

print(lst3)


# Є стрінг з певним текстом (можна скористатися input або константою).
# Напишіть код, який визначить кількість слів в цьому тексті, які закінчуються на 'о'.

str1 = "молоко, дом, вікно, сіро-зелений, молодо-зелено ще тобі"

caseOne = str1.count("о,")
caseTwo = str1.count("о ")

result = caseOne + caseTwo

print(result)

# напишіть функцію, яка перевірить, чи передане їй число є парним чи непарним (повертає True False)

variable_to_check = 3


def check_even_or_odd(a=variable_to_check):
    """
    функція приймає значення, та перевіряє чи є залишок від ділення на два по модулю
    якщо залишок є, то число парне і фунція повертає true, якщо ні, то число непарте і повертає false

    :param a: змінна яку приймає функція для перевірки на парність
    :return True якщо парне, False якщо непарне
    """
    if a % 2 == 0:
        return True
    else:
        return False


check_even_or_odd()

assert type(variable_to_check) == int
assert variable_to_check <= 100
assert variable_to_check >= 0

# напишіть функцію. яка приймає стрічку, і визначає, чи дана стрічка відповідає результатам роботи методу capitalize()
# (перший символ є верхнім регістром, а решта – нижнім.) (повертає True False)
#
# написати до кожної функції мінімум 5 assert

string_to_check = "Хчaa"
converted_string = []


def convert_string_to_list(a=string_to_check):
    """
    Метод перевеодить string to list, роблячи кожен
    символ стрінги окремим елементом list
    Args:
        a: string отримана зі змінної

    Returns: list
    """
    for i in a:
        converted_string.append(i)


def letters_check():
    """
    Цей метод перевіряє перший символ на UpperCase, якщо True їде далі.
    Далі метод перевіряє інші символи на UpperCase чи LowerCase

    Returns: True якщо перший симол UpperCase а інші - LowerCase, Flase якщо ні
    """
    if str(converted_string[0]).isupper():
        for i in converted_string[1:]:
            if i.islower():
                continue
            else:
                return False
        return True
    else:
        return False


convert_string_to_list()
letters_check()

assert string_to_check != str
assert string_to_check != ""
assert converted_string != list
assert letters_check() is True


# написати декоратор, який добавляє принт з результатом роботи отриманої функції + текстовий параметр,
# отриманий ним (декоратор з параметром - це там, де три функції)
# при цьому очікувані результати роботи функції не змінюються (декоратор просто добавляє принт)


def decorator(s):
    """
    Декоратор приймає текстовий параметр та додає принт
    з результатом роботи отриманої функції

    Args:
        s: текстовий параметр що приймає декоратор

    Returns: декоратор повертає принт з результатом роботи
    функції та додає в принт отриаманий теккстовий параметр

    """

    def wrap(function):
        def called(*args, **kwargs):
            print(function(), s)
            return function(*args, **kwargs)

        return called

    return wrap


@decorator('test_text')
def decorated_function():
    """
    This test decorated functon return number 10
    Returns: number 10

    """
    number = 10
    return number


decorated_function()

assert decorated_function != int
assert decorated_function != ""
assert decorator("") != str
assert decorator("") != ""


def makeString(converter):
    """
    Decorator convert int to string and return decorated function
    :param converter: int_to_str, variable with converted value
    :return: wrapper, return decorated function
    """

    def wrapper():
        int_to_string = str(converter())
        return int_to_string

    return wrapper


def user_input(input_value):
    while True:
        try:
            value = int(input(input_value))
        except ValueError:
            print("Try to type numbers, please")
        except KeyboardInterrupt as e:
            print(e)
        else:
            break
    return value


type_value = (user_input("Type some value: "))


@makeString
def intValues():
    int_variable = type_value
    return int_variable


print(type(intValues()))

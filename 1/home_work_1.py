 # Задача: Створіть дві змінні first=10, second=30. Виведіть на екран результат математичної взаємодії (+, -, *, / и тд.) для цих чисел.

firstNumber = 10
secondNumber = 30

result1 = firstNumber + secondNumber
result2 = firstNumber - secondNumber
result3 = firstNumber * secondNumber
result4 = firstNumber ** secondNumber
result5 = firstNumber // secondNumber
result6 = firstNumber % secondNumber

print(result1, result2, result3, result4, result5, result6, '\n')

# Задача: Створіть змінну и по черзі запишіть в неї результат порівняння (<, > , ==, !=) чисел з завдання 1. Виведіть на екран результат кожного порівняння.

equalityResult = firstNumber > secondNumber
print("is", firstNumber, "more than", secondNumber, ":", equalityResult)

equalityResult = firstNumber < secondNumber
print("is", firstNumber, "less than", secondNumber, ":", equalityResult)

equalityResult = firstNumber == secondNumber
print("is", firstNumber, "equal to", secondNumber, ":", equalityResult)

equalityResult = firstNumber != secondNumber
print("is", firstNumber, "not equal to", secondNumber, ":", equalityResult)
